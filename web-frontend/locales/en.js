export default {
  action: {
    back: 'Back',
    backToLogin: 'Back to login',
    signUp: 'Sign up',
    signIn: 'Sign in',
    createNew: 'Create new',
    delete: 'Delete',
    rename: 'Rename',
  },
  adminType: {
    settings: 'Settings',
  },
  applicationType: {
    database: 'Database',
  },
  field: {
    emailAddress: 'E-mail address',
  },
  error: {
    invalidEmail: 'Please enter a valid e-mail address.',
    notMatchingPassword: 'This field must match your password field.',
    minLength: 'A minimum of {min} characters is required here.',
    maxLength: 'A maximum of {max} characters is allowed here.',
  },
}
