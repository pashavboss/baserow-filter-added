export default {
  action: {
    back: 'Retour',
    backToLogin: "Retour à l'identification",
    signUp: 'Créer un compte',
    signIn: "S'identifier",
    createNew: 'Nouveau',
    delete: 'Supprimer',
    rename: 'Renomer',
  },
  adminType: {
    settings: 'Paramètres',
  },
  applicationType: {
    database: 'Base de données',
  },
  field: {
    emailAddress: 'Adresse électronique',
  },
  error: {
    invalidEmail: 'Veuillez entrer une adresse électronique valide.',
    notMatchingPassword: 'Les mots de passe ne correspondent pas.',
    minLength: 'Un minimum de {min} caractères sont attendus ici.',
    maxLength: 'Un maximum de {max} caractères sont attendus ici.',
  },
}
